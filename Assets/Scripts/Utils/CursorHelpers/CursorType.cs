namespace MN.Utils.Cursor
{
	/// <summary>
	///   Cursor types.
	/// </summary>
	public enum CursorType
	{
		Default,
		MouseEnterUI,
		MouseExitUI
	}
}