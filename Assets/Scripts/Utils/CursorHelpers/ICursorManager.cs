namespace MN.Utils.Cursor
{
	/// <summary>
	///   Interface for cursor manager.
	/// </summary>
	public interface ICursorManager
	{
		void SetCursor(CursorType cursorType);
	}
}