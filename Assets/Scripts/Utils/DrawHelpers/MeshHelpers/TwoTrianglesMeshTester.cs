using UnityEngine;

namespace MN.Utils.DrawHelpers.MeshHelpers
{
	public class TwoTrianglesMeshTester : MonoBehaviour
	{
		private MeshFilter _meshFilter;

		private void Awake()
		{
			_meshFilter = GetComponent<MeshFilter>();
		}

		private void Start()
		{
			TwoTrianglesMesh twoTrianglesMesh = new TwoTrianglesMesh();
			_meshFilter.mesh = twoTrianglesMesh.MeshData;
		}
	}
}