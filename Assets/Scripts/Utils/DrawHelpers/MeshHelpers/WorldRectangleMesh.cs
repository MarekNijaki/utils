﻿using MN.Utils.Maths;
using UnityEngine;

namespace MN.Utils.DrawHelpers.MeshHelpers
{
	/// <summary>
	///   Mesh in the world.
	/// </summary>
	public class WorldRectangleMesh
	{
		public GameObject GO { get; private set; }

		private Transform _transform;
		private readonly Material _material;
		private Mesh _mesh;
		private Vector3[] _vertices;
		private Vector2[] _uv;
		private int[] _triangles;

		private const int _LEFT_UPPER_CORNER_INDEX = 0;
		private const int _RIGHT_UPPER_CORNER_INDEX = 1;
		private const int _LEFT_LOWER_CORNER_INDEX = 2;
		private const int _RIGHT_LOWER_CORNER_INDEX = 3;

		/// <summary>
		///   Overloaded constructor.
		/// </summary>
		/// <param name="meshWidth">Width of the mesh</param>
		/// <param name="meshHeight">Height of the mesh</param>
		/// <param name="material">Material to apply to the mesh</param>
		public WorldRectangleMesh(float meshWidth, float meshHeight, Material material) : this(null, meshWidth, meshHeight, material)
		{
			
		}
		
		/// <summary>
		///   Overloaded constructor.
		/// </summary>
		/// <param name="parent">Parent game object transform for the mesh</param>
		/// <param name="meshWidth">Width of the mesh</param>
		/// <param name="meshHeight">Height of the mesh</param>
		/// <param name="material">Material to apply the to mesh</param>
		public WorldRectangleMesh(Transform parent, float meshWidth, float meshHeight, Material material) : 
			     this(parent, meshWidth, meshHeight, material, new UVCoords(0, 0, material.mainTexture.width, material.mainTexture.height))
		{
			
		}
		
		/// <summary>
		///   Overloaded constructor.
		/// </summary>
		/// <param name="lowerLeftCorner">Lower left corner position of the mesh</param>
		/// <param name="upperRightCorner">Upper right corner position of the mesh</param>
		/// <param name="material">Material to apply the to mesh</param>
		public WorldRectangleMesh(Vector3 lowerLeftCorner, Vector3 upperRightCorner, Material material) :
			this(lowerLeftCorner, upperRightCorner, material, new UVCoords(0, 0, material.mainTexture.width, material.mainTexture.height))
		{
			
		}
		
		/// <summary>
		///   Overloaded constructor.
		/// </summary>
		/// <param name="lowerLeftCorner">Lower left corner position of the mesh</param>
		/// <param name="upperRightCorner">Upper right corner position of the mesh</param>
		/// <param name="material">Material to apply the to mesh</param>
		/// <param name="uvCoords">UV coordinates to apply to mesh</param>
		public WorldRectangleMesh(Vector3 lowerLeftCorner, Vector3 upperRightCorner, Material material, UVCoords uvCoords) :
			this(null,upperRightCorner.x - lowerLeftCorner.x, upperRightCorner.y - lowerLeftCorner.y, material, uvCoords)
		{
			Vector3 diagonalDimension = upperRightCorner - lowerLeftCorner;
			Vector3 offsetToReachPivot = diagonalDimension / 2;
			Vector3 pivotPosition = lowerLeftCorner + offsetToReachPivot;
			SetLocalPosition(pivotPosition);
		}
		
		/// <summary>
		///   Main constructor of the mesh.
		/// </summary>
		/// <param name="parent">Parent game object transform for the mesh</param>
		/// <param name="meshWidth">Width of the mesh</param>
		/// <param name="meshHeight">Height of the mesh</param>
		/// <param name="material">Material to apply the to mesh</param>
		/// <param name="uvCoords">UV coordinates to apply to mesh</param>
		public WorldRectangleMesh(Transform parent, float meshWidth, float meshHeight, Material material, UVCoords uvCoords)
		{
			_material = material;

			InitVertices(meshWidth, meshHeight);
			InitUV(uvCoords);
			InitTriangles();
			
			CreateMeshFromData();
			
			CreateMeshGameObject(parent);
			ApplyMeshToGameObject();
		}

		public void SetLocalScale(Vector3 localScale)
		{
			_transform.localScale = localScale;
		}

		public void SetLocalEulerAngles(Vector3 eulerAngles)
		{
			_transform.localEulerAngles = eulerAngles;
		}

		public void SetLocalPosition(Vector3 localPosition)
		{
			_transform.localPosition = localPosition;
		}

		public void MoveLocalPosition(Vector3 move)
		{
			_transform.localPosition += move;
		}
		
		private void InitVertices(float meshWidth, float meshHeight)
		{
			// Take only half of width and height, because mesh will be drawn from origin (0, 0) -> pivot of the mesh will be also in the origin.
			float meshWidthHalf = meshWidth / 2;
			float meshHeightHalf = meshHeight / 2;

			_vertices = new Vector3[4];
			_vertices[_LEFT_UPPER_CORNER_INDEX] = new Vector3(-meshWidthHalf, meshHeightHalf);
			_vertices[_RIGHT_UPPER_CORNER_INDEX] = new Vector3(meshWidthHalf, meshHeightHalf);
			_vertices[_LEFT_LOWER_CORNER_INDEX] = new Vector3(-meshWidthHalf, -meshHeightHalf);
			_vertices[_RIGHT_LOWER_CORNER_INDEX] = new Vector3(meshWidthHalf, -meshHeightHalf);
		}

		private void InitTriangles()
		{
			// Triangles are drawn in clockwise direction.
			// Clockwise order defines front face.
			// Counter-clockwise order defines back face.
			
			_triangles = new int[6];
			
			_triangles[0] = _LEFT_UPPER_CORNER_INDEX;
			_triangles[1] = _RIGHT_UPPER_CORNER_INDEX;
			_triangles[2] = _LEFT_LOWER_CORNER_INDEX;
			
			_triangles[3] = _LEFT_LOWER_CORNER_INDEX;
			_triangles[4] = _RIGHT_UPPER_CORNER_INDEX;
			_triangles[5] = _RIGHT_LOWER_CORNER_INDEX;
		}

		private void InitUV(UVCoords uvCoords)
		{
			_uv = new Vector2[4];
			_uv[_LEFT_UPPER_CORNER_INDEX] = ConvertPixelsToUVCoordinates(uvCoords.LeftUpperCorner);
			_uv[_RIGHT_UPPER_CORNER_INDEX] = ConvertPixelsToUVCoordinates(uvCoords.RightUpperCorner);
			_uv[_LEFT_LOWER_CORNER_INDEX] = ConvertPixelsToUVCoordinates(uvCoords.LeftLowerCorner);
			_uv[_RIGHT_LOWER_CORNER_INDEX] = ConvertPixelsToUVCoordinates(uvCoords.RightLowerCorner);
		}

		private Vector2 ConvertPixelsToUVCoordinates(Pos pos)
		{
			// UV coords is array of normalized Vector2 (left lower corner of the texture is in point (0,0) and right upper corner is in point (1,1)).
			return new Vector2((float)pos.X / _material.mainTexture.width, (float)pos.Y / _material.mainTexture.height);
		}
		
		private void CreateMeshFromData()
		{
			_mesh = new Mesh { vertices = _vertices, uv = _uv, triangles = _triangles };
		}

		private void CreateMeshGameObject(Transform parent)
		{
			GO = new GameObject("Mesh", typeof(MeshFilter), typeof(MeshRenderer));
			_transform = GO.transform;
			_transform.parent = parent;
			_transform.localPosition = Vector3.zero;
			_transform.localScale = Vector3.one;
			_transform.localEulerAngles = Vector3.zero;
		}

		private void ApplyMeshToGameObject()
		{
			GO.GetComponent<MeshFilter>().mesh = _mesh;
			GO.GetComponent<MeshRenderer>().material = _material;
		}
	}
}