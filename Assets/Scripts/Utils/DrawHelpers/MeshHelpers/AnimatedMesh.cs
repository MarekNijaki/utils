using MN.Utils.Timers;
using UnityEngine;

namespace MN.Utils.DrawHelpers.MeshHelpers
{
	public class AnimatedMesh
	{
		public Mesh MeshData { get; private set; }

		private const int _numberOfRectangles = 2;
		private const int _numberOfVerticesInRectangle = 4;
		private const int _numberOfTrianglesInRectangle = 2;
		private const int _numberOfVerticesInTriangle = 3;
		
		private Vector3[] _vertices;
		private Vector2[] _uv;
		private int[] _triangles;
		
		private const int _LEFT_LOWER_CORNER_INDEX = 0;
		private const int _LEFT_UPPER_CORNER_INDEX = 1;
		private const int _RIGHT_UPPER_CORNER_INDEX = 2;
		private const int _RIGHT_LOWER_CORNER_INDEX = 3;
		private const int _LEFT_LOWER_CORNER_INDEX_2 = 4;
		private const int _LEFT_UPPER_CORNER_INDEX_2 = 5;
		private const int _RIGHT_UPPER_CORNER_INDEX_2 = 6;
		private const int _RIGHT_LOWER_CORNER_INDEX_2 = 7;
		
		private const float _textureWidth = 512.0F;
		private const float _textureHeight = 512.0F;
		
		private readonly Vector3 _bodyPosition = new Vector3(0.0F, -50.0F);
		private const float _bodySize = 70.0F;
		private readonly Vector3 _headPosition = new Vector3(6.0F, -12.0F);
		private const float _headSize = 55.0F;

		private bool _isMoveDownAnim = true;
		private float _animHeadPosY = -12.0F;

		public AnimatedMesh()
		{
			InitVertices();
			InitUV();
			InitTriangles();
			ApplyDataToMesh();
			StartHeadAnimation();
		}
		
		private void InitVertices()
		{
			_vertices = new Vector3[_numberOfVerticesInRectangle * _numberOfRectangles];
			
			// Render body.
			_vertices[_LEFT_LOWER_CORNER_INDEX] = _bodyPosition + new Vector3( 0.0F,  0.0F);
			_vertices[_LEFT_UPPER_CORNER_INDEX] = _bodyPosition + new Vector3( 0.0F, _bodySize);
			_vertices[_RIGHT_UPPER_CORNER_INDEX] = _bodyPosition + new Vector3(_bodySize, _bodySize);
			_vertices[_RIGHT_LOWER_CORNER_INDEX] = _bodyPosition + new Vector3(_bodySize,  0);
			
			// Render head.
			_vertices[_LEFT_LOWER_CORNER_INDEX_2] = _headPosition + new Vector3( 0,  0);
			_vertices[_LEFT_UPPER_CORNER_INDEX_2] = _headPosition + new Vector3( 0, _headSize);
			_vertices[_RIGHT_UPPER_CORNER_INDEX_2] = _headPosition + new Vector3(_headSize, _headSize);
			_vertices[_RIGHT_LOWER_CORNER_INDEX_2] = _headPosition + new Vector3(_headSize,  0);
		}

		private void InitUV()
		{
			_uv = new Vector2[_numberOfVerticesInRectangle * _numberOfRectangles];
			
			// Render body.
			Vector2 bodyPixelsLowerLeft = new Vector2(0.0F, 256.0F);
			Vector2 bodyPixelsUpperRight = new Vector2(128.0F, 384.0F);
			// Divide pixel positions of sprite element, by texture dimensions, to get normalize UV value between 0 and 1.
			_uv[_LEFT_LOWER_CORNER_INDEX] = new Vector2(bodyPixelsLowerLeft.x / _textureWidth,  bodyPixelsLowerLeft.y / _textureHeight);
			_uv[_LEFT_UPPER_CORNER_INDEX] = new Vector2(bodyPixelsLowerLeft.x / _textureWidth,  bodyPixelsUpperRight.y / _textureHeight);
			_uv[_RIGHT_UPPER_CORNER_INDEX] = new Vector2(bodyPixelsUpperRight.x / _textureWidth, bodyPixelsUpperRight.y / _textureHeight);
			_uv[_RIGHT_LOWER_CORNER_INDEX] = new Vector2(bodyPixelsUpperRight.x / _textureWidth, bodyPixelsLowerLeft.y / _textureHeight);
			
			// Render head.
			Vector2 headPixelsLowerLeft = new Vector2(0.0F, 384.0F);
			Vector2 headPixelsUpperRight = new Vector2(128.0F, 512.0F);
			// Divide pixel positions of sprite element, by texture dimensions, to get normalize UV value between 0 and 1.
			_uv[_LEFT_LOWER_CORNER_INDEX_2] = new Vector2(headPixelsLowerLeft.x / _textureWidth,  headPixelsLowerLeft.y / _textureHeight);
			_uv[_LEFT_UPPER_CORNER_INDEX_2] = new Vector2(headPixelsLowerLeft.x / _textureWidth,  headPixelsUpperRight.y / _textureHeight);
			_uv[_RIGHT_UPPER_CORNER_INDEX_2] = new Vector2(headPixelsUpperRight.x / _textureWidth, headPixelsUpperRight.y / _textureHeight);
			_uv[_RIGHT_LOWER_CORNER_INDEX_2] = new Vector2(headPixelsUpperRight.x / _textureWidth, headPixelsLowerLeft.y / _textureHeight);
		}

		private void InitTriangles()
		{
			_triangles = new int[_numberOfTrianglesInRectangle * _numberOfVerticesInTriangle* _numberOfRectangles];
			
			// Render body.
			_triangles[0] = _LEFT_LOWER_CORNER_INDEX;
			_triangles[1] = _LEFT_UPPER_CORNER_INDEX;
			_triangles[2] = _RIGHT_UPPER_CORNER_INDEX;

			_triangles[3] = _LEFT_LOWER_CORNER_INDEX;
			_triangles[4] = _RIGHT_UPPER_CORNER_INDEX;
			_triangles[5] = _RIGHT_LOWER_CORNER_INDEX;
			
			// Render head.
			_triangles[6] = _LEFT_LOWER_CORNER_INDEX_2;
			_triangles[7] = _LEFT_UPPER_CORNER_INDEX_2;
			_triangles[8] = _RIGHT_UPPER_CORNER_INDEX_2;

			_triangles[ 9] = _LEFT_LOWER_CORNER_INDEX_2;
			_triangles[10] = _RIGHT_UPPER_CORNER_INDEX_2;
			_triangles[11] = _RIGHT_LOWER_CORNER_INDEX_2;
		}

		private void ApplyDataToMesh()
		{
			MeshData = new Mesh { vertices = _vertices, uv = _uv, triangles = _triangles };
		}

		private void StartHeadAnimation()
		{
			Updater.Create(AnimateHead);
		}
		
		private void AnimateHead()
		{
			if(_isMoveDownAnim) 
			{
				_animHeadPosY += -5f * Time.deltaTime;
				if (_animHeadPosY < -13f) 
				{
					_isMoveDownAnim = false;
				}
			} 
			else 
			{
				_animHeadPosY += + (5.0F * Time.deltaTime);
				if (_animHeadPosY > -10.0F) 
				{
					_isMoveDownAnim = true;
				}
			}

			Vector3 tmpHeadPosition = new Vector3(_headPosition.x, _animHeadPosY);
			_vertices[_LEFT_LOWER_CORNER_INDEX_2] = tmpHeadPosition + new Vector3( 0.0F,  0.0F);
			_vertices[_LEFT_UPPER_CORNER_INDEX_2] = tmpHeadPosition + new Vector3( 0.0F, _headSize);
			_vertices[_RIGHT_UPPER_CORNER_INDEX_2] = tmpHeadPosition + new Vector3(_headSize, _headSize);
			_vertices[_RIGHT_LOWER_CORNER_INDEX_2] = tmpHeadPosition + new Vector3(_headSize,  0.0F);
            
			MeshData.vertices = _vertices;
		}
	}
}