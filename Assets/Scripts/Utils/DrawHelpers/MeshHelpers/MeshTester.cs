using UnityEngine;

namespace MN.Utils.DrawHelpers.MeshHelpers
{
    public class MeshTester : MonoBehaviour
    {
        [SerializeField]
        private Material _material;

        private void Awake()
        {
            GameObject parent = new GameObject("WorldMeshParent");
            parent.transform.position = new Vector3(5, 5, 0);
            WorldRectangleMesh worldRectangleMesh = new WorldRectangleMesh(parent.transform, 5.0F, 5.0F, _material);
            worldRectangleMesh.SetLocalScale(new Vector3(1,1,333));
            
            WorldRectangleMesh worldRectangleMesh2 = new WorldRectangleMesh(new Vector3(-10,-10,0), new Vector3(5,5,0), _material);
        }
    }
}