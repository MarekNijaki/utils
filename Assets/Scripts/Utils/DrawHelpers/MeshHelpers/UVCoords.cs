using MN.Utils.Maths;

namespace MN.Utils.DrawHelpers.MeshHelpers
{
	/// <summary>
	///   Structure for storing UV coords.
	/// </summary>
	public readonly struct UVCoords
	{
		public int X { get; }
		public int Y { get; }
		public int Width { get; }
		public int Height { get; }
		
		public Pos LeftUpperCorner { get; }
		public Pos RightUpperCorner { get; }
		public Pos LeftLowerCorner { get; }
		public Pos RightLowerCorner { get; }

		public UVCoords(int x, int y, int width, int height)
		{
			X = x;
			Y = y;
			Width = width;
			Height = height;

			LeftUpperCorner = new Pos(X, Y + Height);
			RightUpperCorner = new Pos(X + Width, Y + Height);
			LeftLowerCorner = new Pos(X, Y);
			RightLowerCorner = new Pos(X + Width, Y);
		}
	}
}