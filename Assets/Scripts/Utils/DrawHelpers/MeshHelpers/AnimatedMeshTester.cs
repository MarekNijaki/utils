using UnityEngine;

namespace MN.Utils.DrawHelpers.MeshHelpers
{
	public class AnimatedMeshTester : MonoBehaviour
	{
		private MeshFilter _meshFilter;

		private void Awake()
		{
			_meshFilter = GetComponent<MeshFilter>();
		}

		private void Start()
		{
			AnimatedMesh animatedMesh = new AnimatedMesh();
			_meshFilter.mesh = animatedMesh.MeshData;
		}
	}
}