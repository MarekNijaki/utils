using UnityEngine;

namespace MN.Utils.DrawHelpers.MeshHelpers
{
	public class TwoTrianglesMesh
	{
		public Mesh MeshData { get; private set; }
		
		private Vector3[] _vertices;
		private Vector2[] _uv;
		private int[] _triangles;
		
		private const int _LEFT_LOWER_CORNER_INDEX = 0;
		private const int _LEFT_UPPER_CORNER_INDEX = 1;
		private const int _RIGHT_UPPER_CORNER_INDEX = 2;
		private const int _RIGHT_LOWER_CORNER_INDEX = 3;

		public TwoTrianglesMesh()
		{
			InitVertices();
			InitUV();
			InitTriangles();
			ApplyDataToMesh();
		}

		private void InitVertices()
		{
			_vertices = new Vector3[4];
			_vertices[_LEFT_LOWER_CORNER_INDEX] = Vector3.zero;
			_vertices[_LEFT_UPPER_CORNER_INDEX] = new Vector3(0.0F, 100.0F);
			_vertices[_RIGHT_UPPER_CORNER_INDEX] = new Vector3(100.0F, 100.0F);
			_vertices[_RIGHT_LOWER_CORNER_INDEX] = new Vector3(100.0F, 0);
		}

		private void InitUV()
		{
			_uv = new Vector2[4];
			_uv[_LEFT_LOWER_CORNER_INDEX] = Vector3.zero;
			_uv[_LEFT_UPPER_CORNER_INDEX] = Vector2.up;
			_uv[_RIGHT_UPPER_CORNER_INDEX] = Vector2.one;
			_uv[_RIGHT_LOWER_CORNER_INDEX] = Vector2.right;
		}

		private void InitTriangles()
		{
			_triangles = new int[6];
			_triangles[0] = _LEFT_LOWER_CORNER_INDEX;
			_triangles[1] = _LEFT_UPPER_CORNER_INDEX;
			_triangles[2] = _RIGHT_UPPER_CORNER_INDEX;

			_triangles[3] = _LEFT_LOWER_CORNER_INDEX;
			_triangles[4] = _RIGHT_UPPER_CORNER_INDEX;
			_triangles[5] = _RIGHT_LOWER_CORNER_INDEX;
		}

		private void ApplyDataToMesh()
		{
			MeshData = new Mesh { vertices = _vertices, uv = _uv, triangles = _triangles };
		}
	}
}