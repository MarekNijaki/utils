using UnityEngine;

namespace MN.Utils.DrawHelpers.LineHelpers
{
	public class DragLine : MonoBehaviour
	{
		private LineRenderer _lineRenderer;
		private DragLineMover _dragLineMover;

		private void Awake()
		{
			_lineRenderer = GetComponent<LineRenderer>();
			_dragLineMover = FindObjectOfType<DragLineMover>();
		}

		private void Update()
		{
			if(_dragLineMover.IsDragging)
			{
				_lineRenderer.enabled = true;
				_lineRenderer.SetPosition(1, _dragLineMover.transform.position);
			}
			else
			{
				_lineRenderer.enabled = false;
			}
		}
	}
}