using MN.Utils.WorldPositions;
using UnityEngine;

namespace MN.Utils.DrawHelpers.LineHelpers
{
	public class DragLineMover : MonoBehaviour
	{
		public bool IsDragging { get; private set; }

		private SpriteRenderer _spriteRenderer;

		private void Awake()
		{
			_spriteRenderer = GetComponent<SpriteRenderer>();
		}

		private void OnMouseDrag()
		{
			IsDragging = true;
			
			Vector3 worldPos = WorldPosition.GetMouseWorldPosition();
			transform.position = new Vector3(worldPos.x, worldPos.y, transform.position.z);
		}

		private void OnMouseDown()
		{
			_spriteRenderer.color = Color.red;
			IsDragging = false;
		}

		private void OnMouseUp()
		{
			_spriteRenderer.color = Color.white;
		}
	}
}