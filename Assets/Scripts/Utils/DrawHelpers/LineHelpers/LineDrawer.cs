using System.Collections.Generic;
using MN.Utils.WorldPositions;
using UnityEngine;

namespace MN.Utils.DrawHelpers.LineHelpers
{
	public class LineDrawer : MonoBehaviour
	{
		[SerializeField]
		private GameObject _linePrefab;

		private GameObject _currentLineGO;
		private LineRenderer _lineRenderer;
		private EdgeCollider2D _edgeCollider2D;
		private readonly List<Vector2> _points = new List<Vector2>();
		private const float _MIN_DISTANCE_BETWEEN_MOUSE_MOVES = 0.1F;

		private void Update()
		{
			// If left mouse button was pressed (will fire once even if still pressed).
			if(Input.GetMouseButtonDown(0))
			{
				CreateLine();
			}
			
			// If left mouse button is still pressed.
			if(Input.GetMouseButton(0))
			{
				CheckForNewPoint();
			}
		}

		private void CreateLine()
		{
			_currentLineGO = Instantiate(_linePrefab, Vector3.zero, Quaternion.identity);
			_lineRenderer = _currentLineGO.GetComponent<LineRenderer>();
			_edgeCollider2D = _currentLineGO.GetComponent<EdgeCollider2D>();
			_points.Clear();
			
			// Add two points to draw simple dot, before user starts to draw more.
			_points.Add(WorldPosition.GetMouseWorldPosition());
			_points.Add(WorldPosition.GetMouseWorldPosition());
			
			_lineRenderer.SetPosition(0, _points[0]);
			_lineRenderer.SetPosition(1, _points[1]);
			
			_edgeCollider2D.points = _points.ToArray();
		}

		private void CheckForNewPoint()
		{
			Vector2 mouseWorldPosition = WorldPosition.GetMouseWorldPosition();
			if(WasMouseMoved(mouseWorldPosition))
			{
				AddPointToLine(mouseWorldPosition);	
			}
		}

		private bool WasMouseMoved(Vector2 mousePosition)
		{
			return (Vector2.Distance(mousePosition, _points[_points.Count - 1]) > _MIN_DISTANCE_BETWEEN_MOUSE_MOVES);
		}
		
		private void AddPointToLine(Vector2 point)
		{
			_points.Add(point);
			_lineRenderer.positionCount++;
			_lineRenderer.SetPosition(_lineRenderer.positionCount - 1, point);
			_edgeCollider2D.points = _points.ToArray();
		}
	}
}