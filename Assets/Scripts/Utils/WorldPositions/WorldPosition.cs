using UnityEngine;

namespace MN.Utils.WorldPositions
{
	public static class WorldPosition
	{
		/// <summary>
		///   Get position in world based on screen position.
		/// </summary>
		/// <param name="screenPosition">Screen position</param>
		/// <param name="worldCamera">World camera (if not passed will be set to 'Camera.main')</param>
		/// <returns>Position in world based on screen position</returns>
		public static Vector3 ScreenPositionToWorldPosition(Vector3 screenPosition, Camera worldCamera = null)
		{
			if(worldCamera == null)
				worldCamera = Camera.main;
			
			Vector3 worldPosition = worldCamera.ScreenToWorldPoint(screenPosition);
			return worldPosition;
		}
		
		public static Vector3 ScreenPositionToWorldPositionWithZ(Vector3 screenPosition, Camera worldCamera = null)
		{
			if(worldCamera == null)
				worldCamera = Camera.main;

			// If 'z' is set to 0, raycast distance will be 0, so function would always return camera position as screen
			// position, what is incorrect behaviour. To fix that we use camera near clip plane as 'z' distance to shoot raycast.
			Debug.Log("x" +screenPosition.z);
			if(screenPosition.z == 0)
			{
				Debug.Log("asdasd" +worldCamera.nearClipPlane);
				screenPosition.z = worldCamera.nearClipPlane;
				screenPosition.z = Mathf.Abs(worldCamera.transform.position.z);
				screenPosition.z = worldCamera.farClipPlane;
			}
			
			Vector3 worldPosition = worldCamera.ScreenToWorldPoint(screenPosition);
			return worldPosition;
		}
		
		/// <summary>
		///   Get mouse position in world.
		/// </summary>
		/// <param name="worldCamera">World camera (if not passed will be set to 'Camera.main')</param>
		/// <returns>Mouse world position</returns>
		public static Vector3 GetMouseWorldPosition(Camera worldCamera = null)
		{
			return ScreenPositionToWorldPosition(Input.mousePosition, worldCamera);
		}
		
		public static Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z) 
		{
			Ray ray = Camera.main.ScreenPointToRay(screenPosition);
			Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
			float distance;
			xy.Raycast(ray, out distance);
			return ray.GetPoint(distance);
		}
		
		public static Vector3 GetXXX() 
		{
			Plane plane = new Plane(Vector3.up, 0);

			float distance;
			Vector3 worldPosition = Vector3.zero;
			Vector3 mousPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
			Vector3 p = Camera.main.ViewportToWorldPoint(new Vector3(mousPos.x, mousPos.y, Camera.main.nearClipPlane));
			
			return p;
		}
		
		public static Vector3 GetXXX2() 
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitData;

			Vector3 worldPosition = Vector3.zero;
			if(Physics.Raycast(ray, out hitData, 1000))
			{
				worldPosition = hitData.point;
			}
			
			return worldPosition;
		}
		
		/// <summary>
		///   Get mouse position in world (ignore 'z' axis).
		/// </summary>
		/// <returns>Mouse world position without 'z' axis</returns>
		public static Vector3 GetMouseWorldPositionWithoutZ(Camera worldCamera = null)
		{
			Vector3 position = GetMouseWorldPosition(worldCamera);
			position.z = 0.0F;
			
			return position;
		}
		
		/// <summary>
		///   Get UI Position from World Position.
		/// </summary>
		/// <param name="worldPosition"></param>
		/// <param name="parent"></param>
		/// <param name="UICamera"></param>
		/// <param name="worldCamera"></param>
		/// <returns></returns>
		public static Vector2 GetWorldUIPosition(Vector3 worldPosition, Transform parent, Camera UICamera, Camera worldCamera)
		{
			Vector3 screenPosition = worldCamera.WorldToScreenPoint(worldPosition);
			Vector3 UICameraWorldPosition = UICamera.ScreenToWorldPoint(screenPosition);
			Vector3 localPos = parent.InverseTransformPoint(UICameraWorldPosition);
			
			return new Vector2(localPos.x, localPos.y);
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static Vector3 GetWorldPositionFromUIPerspective()
		{
			return GetWorldPositionFromUIPerspective(Input.mousePosition, Camera.main);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="worldCamera"></param>
		/// <returns></returns>
		public static Vector3 GetWorldPositionFromUIPerspective(Camera worldCamera)
		{
			return GetWorldPositionFromUIPerspective(Input.mousePosition, worldCamera);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="screenPosition"></param>
		/// <param name="worldCamera"></param>
		/// <returns></returns>
		public static Vector3 GetWorldPositionFromUIPerspective(Vector3 screenPosition, Camera worldCamera)
		{
			Plane plane = new Plane(Vector3.forward, new Vector3(0, 0, 0f));
			
			Ray ray = worldCamera.ScreenPointToRay(screenPosition);
			plane.Raycast(ray, out float distance);
			Vector3 worldPosition = ray.GetPoint(distance);
			
			return worldPosition;
		}
	}
}