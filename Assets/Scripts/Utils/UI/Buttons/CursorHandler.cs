using MN.Utils.Cursor;
using UnityEngine;

namespace MN.Utils.UI.Buttons
{
	/// <summary>
	///   Handler for cursor.
	/// </summary>
	public class CursorHandler : MonoBehaviour
	{
		[SerializeField]
		private CursorType _cursorTypeForMouseOver;
		[SerializeField]
		private CursorType _cursorTypeForMouseOut;

		private ICursorManager _cursorManager;
		
		private void OnMouseEnter()
		{
			_cursorManager.SetCursor(_cursorTypeForMouseOver);
		}
		
		private void OnMouseExit()
		{
			_cursorManager.SetCursor(_cursorTypeForMouseOut);
		}
	}
}