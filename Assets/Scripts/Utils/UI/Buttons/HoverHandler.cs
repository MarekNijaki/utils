using System;
using UnityEngine;

namespace MN.Utils.UI.Buttons
{
	/// <summary>
	///   Handler for hover.
	/// </summary>
	public class HoverHandler
	{
		public HoverBehaviourType hoverBehaviourType = HoverBehaviourType.Custom;

		public Color ColorOnEnter = new Color(1, 1, 1, 1);
		public Color ColorOnExit = new Color(1, 1, 1, 1);
		public SpriteRenderer SpriteRenderer;
		public Sprite SpriteOnExit;
		public Sprite SpriteOnEnter;
		public bool HasHoverOffset;
		public Vector2 HoverOffset = Vector2.zero;
		
		private Action FuncOnEnter;
		private Action FuncOnExit;
		
		private Vector3 posEnter;
		private Vector3 posExit;
		
		
		// private void Awake()
		// {
		// 	posExit = transform.localPosition;
		// 	posEnter = transform.localPosition + (Vector3)HoverOffset;
		// 	Setup();
		// }
		//
		// public void SetChangeColor(Color colorOnEnter, Color colorOnOut)
		// {
		// 	hoverBehaviourType = HoverBehaviourType.ChangeColor;
		// 	ColorOnEnter = colorOnEnter;
		// 	ColorOnExit = colorOnOut;
		// 	
		// 	if(SpriteRenderer == null)
		// 	{
		// 		SpriteRenderer = transform.GetComponent<SpriteRenderer>();
		// 	}
		// 	SpriteRenderer.color = ColorOnExit;
		// 	
		// 	Setup();
		// }
		//
		// private void Setup()
		// {
		// 	switch(hoverBehaviourType)
		// 	{
		// 		case HoverBehaviourType.ChangeColor:
		// 		{
		// 			FuncOnEnter = delegate() { SpriteRenderer.color = ColorOnEnter; };
		// 			FuncOnExit = delegate() { SpriteRenderer.color = ColorOnExit; };
		// 			break;
		// 		}
		// 		case HoverBehaviourType.ChangeImage:
		// 		{
		// 			FuncOnEnter = delegate() { SpriteRenderer.sprite = SpriteOnEnter; };
		// 			FuncOnExit = delegate() { SpriteRenderer.sprite = SpriteOnExit; };
		// 			break;
		// 		}
		// 		case HoverBehaviourType.ChangeSetActive:
		// 		{
		// 			FuncOnEnter = delegate() { SpriteRenderer.gameObject.SetActive(true); };
		// 			FuncOnExit = delegate() { SpriteRenderer.gameObject.SetActive(false); };
		// 			break;
		// 		}
		// 	}
		// }
		//
		// private void OnMouseEnter()
		// {
		// 	if(HasHoverOffset)
		// 	{
		// 		transform.localPosition = posEnter;
		// 	}
		// 	
		// 	if(FuncOnEnter != null)
		// 	{
		// 		FuncOnEnter();
		// 	}
		// }
		//
		//
		// private void OnMouseExit()
		// {
		// 	if(HasHoverOffset)
		// 	{
		// 		transform.localPosition = posExit;
		// 	}
		// 	
		// 	if(FuncOnExit != null)
		// 	{
		// 		FuncOnExit();
		// 	}
		// }
	}
}