using MN.Utils.Audio;
using UnityEngine;

namespace MN.Utils.UI.Buttons
{
	/// <summary>
	///   Handler for sound.
	/// </summary>
	public class SoundHandler : MonoBehaviour
	{
		[SerializeField]
		private AudioClip OnMouseOverSound;
		[SerializeField]
		public AudioClip OnMouseClickSound;

		private IAudioManager _audioManager;
		
		private void OnMouseEnter()
		{
			_audioManager.Play(OnMouseOverSound);
		}
		
		private void OnMouseDown()
		{
			_audioManager.Play(OnMouseClickSound);
		}
	}
}