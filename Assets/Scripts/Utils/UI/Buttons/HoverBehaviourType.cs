namespace MN.Utils.UI.Buttons
{
    /// <summary>
    ///   Hover behaviour types.
    /// </summary>
    public enum HoverBehaviourType
    {
        Custom,
        ChangeColor,
        ChangeImage,
        ChangeSetActive,
    }
}