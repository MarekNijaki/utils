﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MN.Utils.UI.Buttons
{
	/// <summary>
	///   Button on a world BoxCollider.
	/// </summary>
	public class Button_Sprite : MonoBehaviour
	{
		public Action ClickFunc = null;
		public Action MouseRightDownOnceFunc = null;
		public Action MouseRightDownFunc = null;
		public Action MouseRightUpFunc = null;
		public Action MouseDownOnceFunc = null;
		public Action MouseUpOnceFunc = null;
		public Action MouseOverOnceFunc = null;
		public Action MouseOutOnceFunc = null;
		public Action MouseOverOnceTooltipFunc = null;
		public Action MouseOutOnceTooltipFunc = null;

		private bool draggingMouseRight;
		private Vector3 mouseRightDragStart;
		public Action<Vector3, Vector3> MouseRightDragFunc = null;
		public Action<Vector3, Vector3> MouseRightDragUpdateFunc = null;
		public bool triggerMouseRightDragOnEnter;

		
		private static Func<Camera> _worldCamera;
		
		public bool ShouldTriggerMouseOutFuncOnClick;
		public bool CanClickThroughUI;
		
		
		public static void SetWorldCamera(Func<Camera> getWorldCamera)
		{
			_worldCamera = getWorldCamera;
		}

		public void ManualOnMouseExit()
		{
			OnMouseExit();
		}
		
		private void Awake()
		{
			if(_worldCamera == null)
			{
				// Set default World Camera
				SetWorldCamera(() => Camera.main); 
			}
		}

		private void OnMouseDown()
		{
			if(!CanClickThroughUI && IsPointerOverUI())
			{
				return;
			}
			
			if(ClickFunc != null)
			{
				ClickFunc();
			}
			
			if(ShouldTriggerMouseOutFuncOnClick)
			{
				OnMouseExit();
			}
		}

		private void OnMouseUp()
		{
			if(MouseUpOnceFunc != null)
			{
				MouseUpOnceFunc();
			}
		}

		private void OnMouseEnter()
		{
			if(!CanClickThroughUI && IsPointerOverUI())
			{
				return; 
			}
			
			if(MouseOverOnceFunc != null)
			{
				MouseOverOnceFunc();
			}
			
			if(MouseOverOnceTooltipFunc != null)
			{
				MouseOverOnceTooltipFunc();
			}
		}

		private void OnMouseExit()
		{
			if(MouseOutOnceFunc != null)
			{
				MouseOutOnceFunc();
			}
			
			if(MouseOutOnceTooltipFunc != null)
			{
				MouseOutOnceTooltipFunc();
			}
		}

		private void OnMouseOver()
		{
			if(!CanClickThroughUI && IsPointerOverUI())
			{
				return; 
			}

			if(Input.GetMouseButton(1))
			{
				if(MouseRightDownFunc != null)
				{
					MouseRightDownFunc();
				}
				if(!draggingMouseRight && triggerMouseRightDragOnEnter)
				{
					draggingMouseRight = true;
					mouseRightDragStart = GetWorldPositionFromUI();
				}
			}
			
			if(Input.GetMouseButtonDown(1))
			{
				draggingMouseRight = true;
				mouseRightDragStart = GetWorldPositionFromUI();
				if(MouseRightDownOnceFunc != null)
				{
					MouseRightDownOnceFunc();
				}
			}
		}

		private void Update()
		{
			if(draggingMouseRight)
			{
				if(MouseRightDragUpdateFunc != null)
				{
					MouseRightDragUpdateFunc(mouseRightDragStart, GetWorldPositionFromUI());
				}
			}
			
			if(Input.GetMouseButtonUp(1))
			{
				if(draggingMouseRight)
				{
					draggingMouseRight = false;
					if(MouseRightDragFunc != null)
					{
						MouseRightDragFunc(mouseRightDragStart, GetWorldPositionFromUI());
					}
				}
				if(MouseRightUpFunc != null)
				{
					MouseRightUpFunc();
				}
			}
		}

		private static Vector3 GetWorldPositionFromUI()
		{
			Vector3 worldPosition = _worldCamera().ScreenToWorldPoint(Input.mousePosition);
			return worldPosition;
		}

		private bool IsPointerOverUI()
		{
			if(EventSystem.current.IsPointerOverGameObject())
			{
				return true;
			}
			
			PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
			pointerEventData.position = Input.mousePosition;
			List<RaycastResult> hits = new List<RaycastResult>();
			EventSystem.current.RaycastAll(pointerEventData, hits);
			
			return hits.Count > 0;
		}
	}
}