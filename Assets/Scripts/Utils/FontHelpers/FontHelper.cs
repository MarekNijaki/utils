using UnityEngine;

namespace MN.Utils.FontHelpers
{
	/// <summary>
	///   Helper class for font.
	/// </summary>
	public static class FontHelper
	{
		/// <summary>
		///   Get Default Unity Font.
		///   <para></para>
		///   Used in text objects, if no font was given there.
		/// </summary>
		/// <returns></returns>
		public static Font GetDefaultFont()
		{
			return Resources.GetBuiltinResource<Font>("Arial.ttf");
		}
	}
}