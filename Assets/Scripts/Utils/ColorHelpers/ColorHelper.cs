using UnityEngine;
using MN.Utils.Converters;

namespace MN.Utils.ColorHelpers
{
	public static class ColorHelper
	{
		/// <summary>
		///   Get hexadecimal strings from color, for each channel (FF).
		/// </summary>
		/// <param name="color">Color to process</param>
		/// <param name="red">Red chanel value</param>
		/// <param name="green">Green chanel value</param>
		/// <param name="blue">Blue chanel value</param>
		/// <param name="alpha">Alpha chanel value</param>
		public static void GetChanelStringsFromColor(Color color, out string red, out string green, out string blue, out string alpha)
		{
			red = Converter.Dec01ToHex(color.r);
			green = Converter.Dec01ToHex(color.g);
			blue = Converter.Dec01ToHex(color.b);
			alpha = Converter.Dec01ToHex(color.a);
		}

		/// <summary>
		///   Get hexadecimal string from color channels (FF00FF).
		///   <para></para>
		///   Alpha value NOT included.
		/// </summary>
		/// <param name="red">Red chanel value</param>
		/// <param name="green">Green chanel value</param>
		/// <param name="blue">Blue chanel value</param>
		/// <returns>String representing color as hexadecimal value (alpha value NOT included)</returns>
		public static string GetStringFromColor(float red, float green, float blue)
		{
			string r = Converter.Dec01ToHex(red);
			string g = Converter.Dec01ToHex(green);
			string b = Converter.Dec01ToHex(blue);
			return r + g + b;
		}

		/// <summary>
		///   Get hexadecimal string from color channels (FF00FFAA).
		///   <para></para>
		///   Alpha value included.
		/// </summary>
		/// <param name="red">Red chanel value</param>
		/// <param name="green">Green chanel value</param>
		/// <param name="blue">Blue chanel value</param>
		/// <param name="alpha">Alpha chanel value</param>
		/// <returns>String representing color as hexadecimal value (alpha value included)</returns>
		public static string GetStringFromColor(float red, float green, float blue, float alpha)
		{
			string a = Converter.Dec01ToHex(alpha);
			return GetStringFromColor(red, green, blue) + a;
		}
		
		/// <summary>
		///   Get hexadecimal string from color (FF00FF).
		///   <para></para>
		///   Alpha value NOT included.
		/// </summary>
		/// <param name="color">Color to process</param>
		/// <returns>String representing color as hexadecimal value (alpha value NOT included)</returns>
		public static string GetStringFromColor(Color color)
		{
			return GetStringFromColor(color.r, color.g, color.b);
		}

		/// <summary>
		///   Get hexadecimal string from color with alpha value (FF00FFAA).
		///   <para></para>
		///   Alpha value included.
		/// </summary>
		/// <param name="color">Color to process</param>
		/// <returns>String representing color as hexadecimal value (alpha value included)</returns>
		public static string GetStringFromColorWithAlpha(Color color)
		{
			return GetStringFromColor(color.r, color.g, color.b, color.a);
		}

		/// <summary>
		///   Get color from string with hexadecimal color value (FF00FFAA).
		/// </summary>
		/// <param name="colorTxt">String with color value</param>
		/// <returns>Color</returns>
		public static Color GetColorFromString(string colorTxt)
		{
			float red = Converter.HexToDec01(colorTxt.Substring(0, 2));
			float green = Converter.HexToDec01(colorTxt.Substring(2, 2));
			float blue = Converter.HexToDec01(colorTxt.Substring(4, 2));
			float alpha = 1.0F;
			
			// If color string contains alpha.
			if(colorTxt.Length >= 8)
			{
				alpha = Converter.HexToDec01(colorTxt.Substring(6, 2));
			}
			
			return new Color(red, green, blue, alpha);
		}
	}
}