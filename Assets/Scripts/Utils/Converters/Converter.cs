using System;
using UnityEngine;

namespace MN.Utils.Converters
{
	public static class Converter
	{
		/// <summary>
		///   Convert decimal value to string with hexadecimal value.
		///   <para></para>
		///   IN: decimal value between 0 - 255.
		///   <para></para>
		///   OUT: string with hexadecimal value between 00 - FF.
		/// </summary>
		/// <param name="decimalValue">Decimal value to convert</param>
		/// <returns>String with hexadecimal value</returns>
		public static string DecToHex(int decimalValue)
		{
			return decimalValue.ToString("X2");
		}

		/// <summary>
		///   Convert decimal value to string with hexadecimal value.
		///   <para></para>
		///   IN: decimal value between 0 - 1.
		///   <para></para>
		///   OUT: string with hexadecimal value between 00 - FF.
		/// </summary>
		/// <param name="decimalValue">Decimal value to convert</param>
		/// <returns>String with hexadecimal value</returns>
		public static string Dec01ToHex(float decimalValue)
		{
			return DecToHex((int)Mathf.Round(decimalValue * 255f));
		}

		/// <summary>
		///   Convert string with hexadecimal value to decimal value.
		///   <para></para>
		///   IN: string with hexadecimal value between 00 - FF.
		///   <para></para>
		///   OUT: decimal value between 0 - 255.
		/// </summary>
		/// <param name="hexadecimalValue">Hexadecimal value to convert</param>
		/// <returns>Decimal value</returns>
		public static int HexToDec(string hexadecimalValue)
		{
			return Convert.ToInt32(hexadecimalValue, 16);
		}
		
		/// <summary>
		///   Convert string with hexadecimal value to decimal value.
		///   <para></para>
		///   IN: string with hexadecimal value between 00 - FF.
		///   <para></para>
		///   OUT: decimal float value between 0 - 1.
		/// </summary>
		/// <param name="hexadecimalValue">Hexadecimal value to convert</param>
		/// <returns>Decimal value</returns>
		public static float HexToDec01(string hexadecimalValue)
		{
			return HexToDec(hexadecimalValue) / 255f;
		}
	}
}