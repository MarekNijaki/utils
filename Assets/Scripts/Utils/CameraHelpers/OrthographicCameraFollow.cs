﻿using System;
using UnityEngine;

namespace MN.Utils.CameraHelpers
{
	/// <summary>
	///   Script to handle Camera Movement and Zoom.
	///   <para></para>
	///   Place on Camera GameObject.
	/// </summary>
	public class OrthographicCameraFollow : MonoBehaviour
	{
		private const float _MOVE_SPEED = 3.0F;
		private const float _ZOOM_SPEED = 1.0F;
		private const float _Z_OFFSET = 10.0F;

		private Camera _camera;
		private Func<Vector3> _GetFollowedObjectPosition;
		private Func<float> _GetZoom;

		private void Start()
		{
			_camera = transform.GetComponent<Camera>();
		}

		private void Update()
		{
			RemoveZFactor();
			HandleMovement();
			HandleZoom();
			ApplyZOffset();
		}

		public void Setup(Func<Vector3> GetFollowedObjectPosition, Func<float> GetZoom)
		{
			_GetFollowedObjectPosition = GetFollowedObjectPosition;
			_GetZoom = GetZoom;
		}

		private void RemoveZFactor()
		{
			transform.position = new Vector3(transform.position.x, transform.position.y, _GetFollowedObjectPosition().z);
		}

		private void HandleMovement()
		{
			float distance = GetDistanceTowardsFollowedObject();
			if(ShouldCameraMove(distance))
			{
				ApplyMovement(distance);
			}
		}

		private float GetDistanceTowardsFollowedObject()
		{
			return Vector3.Distance(_GetFollowedObjectPosition(), transform.position);
		}

		private static bool ShouldCameraMove(float distance)
		{
			return (distance > 0);
		}

		private void ApplyMovement(float distance)
		{
			transform.position = GetCameraNewPosition(distance);
		}

		private Vector3 GetCameraNewPosition(float distance)
		{
			Vector3 moveDirection = GetNormalizedDirectionTowardsFollowedObject();
			Vector3 newPosition = transform.position + moveDirection * (distance * _MOVE_SPEED * Time.deltaTime);
			if(WillOvershootPosition(distance, newPosition))
			{
				newPosition = _GetFollowedObjectPosition();
			}

			return newPosition;
		}

		private Vector3 GetNormalizedDirectionTowardsFollowedObject()
		{
			return (_GetFollowedObjectPosition() - transform.position).normalized;
		}

		private bool WillOvershootPosition(float distance, Vector3 newPosition)
		{
			return Vector3.Distance(newPosition, _GetFollowedObjectPosition()) > distance;
		}

		private void HandleZoom()
		{
			float newZoom = GetNewZoom();
			ApplyZoom(newZoom);
		}

		private float GetNewZoom()
		{
			float zoomDifference = GetZoomDifference();
			float newZoom = _camera.orthographicSize + zoomDifference * _ZOOM_SPEED * Time.deltaTime;
			if(WillOvershootZoom(newZoom, zoomDifference))
			{
				newZoom = _GetZoom();
			}

			return newZoom;
		}

		private float GetZoomDifference()
		{
			return _GetZoom() - _camera.orthographicSize;
		}

		private bool WillOvershootZoom(float newZoom, float zoomDifference)
		{
			if(zoomDifference > 0)
			{
				if(newZoom > _GetZoom())
				{
					return true;
				}
			}
			else
			{
				if(newZoom < _GetZoom())
				{
					return true;
				}
			}

			return false;
		}

		private void ApplyZoom(float newZoom)
		{
			_camera.orthographicSize = newZoom;
		}

		private void ApplyZOffset()
		{
			transform.position = new Vector3(transform.position.x, transform.position.y, _GetFollowedObjectPosition().z - _Z_OFFSET);
		}
	}
}