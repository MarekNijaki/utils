using MN.Utils.Maths;
using MN.Utils.Timers;
using UnityEngine;

namespace MN.Utils.CameraHelpers
{
	public static class CameraHelper
	{
		private static float _shakeTimeLeft;
		private static Vector3 _previousMove = Vector3.zero;
		private static Transform _transform;
		
		/// <summary>
		///   Camera shake.
		/// </summary>
		/// <param name="intensity">Intensity of shake</param>
		/// <param name="time">Shake time</param>
		public static void ShakeCamera(float intensity, float time)
		{
			_shakeTimeLeft = time;
			_transform = Camera.main.transform;
			_previousMove = Vector3.zero;
				
			Updater.Create(() => CameraShakeInternal(intensity),"ShakeCamera()", true, true);
		}

		/// <summary>
		///   Internal camera shake.
		/// </summary>
		/// <param name="intensity">Intensity of shake</param>
		/// <returns></returns>
		private static bool CameraShakeInternal(float intensity)
		{
			_shakeTimeLeft -= Time.unscaledDeltaTime;

			Vector3 randomMove = GetRandomMove(intensity);
			randomMove = NullifyLastMovement(randomMove);
			_transform.position += randomMove;
			
			return IsShakeFinished();
		}

		/// <summary>
		///   Get random move.
		/// </summary>
		/// <param name="intensity"></param>
		/// <returns></returns>
		private static Vector3 GetRandomMove(float intensity)
		{
			return MathGeometry.GetRandomDirection() * intensity;;
		}

		/// <summary>
		///   Nullify previous shake movement, so it will not make problems with next shake movement.
		/// </summary>
		/// <param name="move">Move vector to process</param>
		/// <returns>Move vector subtracted by previous shake move vector</returns>
		private static Vector3 NullifyLastMovement(Vector3 move)
		{
			move -= _previousMove;
			_previousMove = move;

			return move;
		}

		/// <summary>
		///   Return flag if shake of camera is finished.
		/// </summary>
		/// <returns>Flag if shake of camera is finished</returns>
		private static bool IsShakeFinished()
		{
			return _shakeTimeLeft <= 0.0F;
		}
	}
}