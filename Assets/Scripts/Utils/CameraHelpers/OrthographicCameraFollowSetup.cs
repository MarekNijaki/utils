﻿using UnityEngine;

namespace MN.Utils.CameraHelpers
{
	/// <summary>
	///   Easy set up for CameraFollow.
	///   <para></para>
	///   Camera will follow given transform, with passed zoom value.
	/// </summary>
	public class OrthographicCameraFollowSetup : MonoBehaviour
	{
		[SerializeField]
		private OrthographicCameraFollow _cameraFollow;
		[SerializeField]
		private Transform _followedTransform;
		[SerializeField]
		private float _zoom = 7.0F;

		private void Start()
		{
			if(_followedTransform == null)
			{
				Debug.LogError("followTransform is null! Intended?");
				_cameraFollow.Setup(GetNullFollowedObjectPosition, GetCameraZoom);
			}
			else
			{
				_cameraFollow.Setup(GetFollowedObjectPosition, GetCameraZoom);
			}
		}

		private Vector3 GetFollowedObjectPosition()
		{
			return _followedTransform.position;
		}

		private Vector3 GetNullFollowedObjectPosition()
		{
			return Vector3.zero;
		}

		private float GetCameraZoom()
		{
			return _zoom;
		}
	}
}