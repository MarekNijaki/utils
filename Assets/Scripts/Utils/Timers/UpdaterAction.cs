using System;
using UnityEngine;

namespace MN.Utils.Timers
{
	/// <summary>
	///   Calls function on every Update until it returns true.
	/// </summary>
	public class UpdaterAction
	{
		public GameObject GO { get; }
		public string Name { get; }
		public bool Active { get; private set; }
		
		/// <summary>
		///   Function to run on each update tick.
		///   <para></para>
		///   Destroy updater action if function to update returns true.
		/// </summary>
		private readonly Func<bool> _updateFunc;
		
		/// <summary>
		///   Create function that should be run on every update tick.
		/// </summary>
		/// <param name="updateFunc">Function to run on each update tick</param>
		/// <param name="name">Name of update action</param>
		/// <param name="active">Flag if update action should be active</param>
		/// <returns>Created update action</returns>
		public static UpdaterAction Create(Func<bool> updateFunc, string name, bool active)
		{
			GameObject updaterActionGO = new GameObject("UpdaterAction [" + name + "]", typeof(MonoBehaviourHook));
			UpdaterAction updaterAction = new UpdaterAction(updaterActionGO, name, active, updateFunc);
			updaterActionGO.GetComponent<MonoBehaviourHook>().onUpdate = updaterAction.UpdateTick;
			
			return updaterAction;
		}
		
		/// <summary>
		///   Pause work of update action.
		/// </summary>
		public void Pause()
		{
			Active = false;
		}

		/// <summary>
		///   Resume work of update action.
		/// </summary>
		public void Resume()
		{
			Active = true;
		}
		
		/// <summary>
		///   Constructor of update action.
		/// </summary>
		/// <param name="gameObject">Game object to which to attach update action</param>
		/// <param name="name">Name of update action</param>
		/// <param name="active">Flag if update action should be active</param>
		/// <param name="updateFunc">Function to run on each update tick</param>
		private UpdaterAction(GameObject gameObject, string name, bool active, Func<bool> updateFunc)
		{
			GO = gameObject;
			Name = name;
			Active = active;
			_updateFunc = updateFunc;
		}

		/// <summary>
		///   Update tick.
		/// </summary>
		private void UpdateTick()
		{
			if(!Active)
			{
				return;
			}
			
			if(ShouldBeRemoved())
			{
				Updater.Remove(this);
			}
		}

		/// <summary>
		///   Check if update action should be removed.
		/// </summary>
		/// <returns>Flag if update action should be removed</returns>
		private bool ShouldBeRemoved()
		{
			return _updateFunc() == true;
		}
	}
}