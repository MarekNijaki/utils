using System;
using System.Collections.Generic;
using UnityEngine;

namespace MN.Utils.Timers
{
	/// <summary>
	///   Entry point for update actions.
	/// </summary>
	public static class Updater
	{
		/// <summary>
		///   Game object used as a flag, to determine if updater was initialized after loading scene.
		/// </summary>
		private static GameObject _isInitializedGOFlag;
		private static List<UpdaterAction> _updaterActions;

		/// <summary>
		///   Create function that should be run on every update tick.
		/// </summary>
		/// <param name="updateFunc">Function to run on each update tick</param>
		/// <returns>Created update action</returns>
		public static UpdaterAction Create(Action updateFunc)
		{
			bool UpdateActionWrapper() 
			{
				updateFunc();
				return false;
			}
			
			return Create(UpdateActionWrapper);
		}

		/// <summary>
		///   Create function that should be run on every update tick.
		/// </summary>
		/// <param name="updateFunc">Function to run on each update tick</param>
		/// <param name="name">Name of update action</param>
		/// <param name="active">Flag if update action should be active</param>
		/// <param name="shouldRemoveActionsWithSameName">Flag if actions of the same name, should be removed before creation of this action</param>
		/// <returns>Created update action</returns>
		public static UpdaterAction Create(Func<bool> updateFunc, string name = "", bool active = true, bool shouldRemoveActionsWithSameName = false)
		{
			InitIfNeeded();

			if(shouldRemoveActionsWithSameName)
			{
				RemoveUpdaterActionsOfGivenName(name);
			}

			UpdaterAction action = UpdaterAction.Create(updateFunc, name, active);
			_updaterActions.Add(action);
			
			return action;
		}

		/// <summary>
		///   Remove passed updater action.
		/// </summary>
		/// <param name="updaterAction">Updater action to remove</param>
		public static void Remove(UpdaterAction updaterAction)
		{
			InitIfNeeded();
			
			_updaterActions.Remove(updaterAction);
			
			if(updaterAction.GO != null)
			{
				UnityEngine.Object.Destroy(updaterAction.GO);
			}
		}
		
		/// <summary>
		///   Remove all updater actions of given name.
		/// </summary>
		/// <param name="name">Name of updater actions to remove</param>
		public static void RemoveUpdaterActionsOfGivenName(string name)
		{
			InitIfNeeded();
			
			for(int i = _updaterActions.Count - 1; i > -1; i--)
			{
				if(_updaterActions[i].Name != name)
				{
					continue;
				}
				
				Remove(_updaterActions[i]);
			}
		}
		 
		/// <summary>
		///   Initialise updater if needed.
		///   <para></para>
		///   Updater is initialised once on given scene, when first used.
		/// </summary>
		private static void InitIfNeeded()
		{
			if(_isInitializedGOFlag != null)
			{
				return;
			}
			
			_isInitializedGOFlag = new GameObject("UpdaterInitFlag");
			_updaterActions = new List<UpdaterAction>();
		}
	}
}