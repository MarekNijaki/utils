using System;
using UnityEngine;

namespace MN.Utils.Timers
{
	/// <summary>
	///   Triggers an action after a certain time.
	/// </summary>
	public class TimerAction
	{
		public GameObject GO { get; }
		public string Name { get; }
		
		private readonly bool _useUnscaledDeltaTime;
		private float _timeLeft;
		private readonly Action _action;
		
		/// <summary>
		///   Create action that should be triggered after certain time.
		/// </summary>
		/// <param name="name">Name of timer action</param>
		/// <param name="useUnscaledDeltaTime">Flag if timer should use unscaled delta time</param>
		/// <param name="time">Time to wait unlit trigger action</param>
		/// <param name="action">Action to trigger after time elapse</param>
		/// <returns>Created timer action</returns>
		public static TimerAction Create(string name, bool useUnscaledDeltaTime, float time, Action action)
		{
			GameObject timerActionGO = new GameObject("TimerAction [" + name + "]", typeof(MonoBehaviourHook));
			TimerAction timerAction = new TimerAction(timerActionGO, name, useUnscaledDeltaTime, time, action);
			timerActionGO.GetComponent<MonoBehaviourHook>().onUpdate = timerAction.UpdateTick;

			return timerAction;
		}
		
		/// <summary>
		///   Constructor of timer action.
		/// </summary>
		/// <param name="gameObject">Game object to which to attach timer action</param>
		/// <param name="name">Name of timer action</param>
		/// <param name="useUnscaledDeltaTime">Flag if timer should use unscaled delta time</param>
		/// <param name="time">Time to wait unlit trigger action</param>
		/// <param name="action">Action to trigger after time elapse</param>
		private TimerAction(GameObject gameObject, string name, bool useUnscaledDeltaTime, float time, Action action)
		{
			GO = gameObject;
			Name = name;
			_useUnscaledDeltaTime = useUnscaledDeltaTime;
			_timeLeft = time;
			_action = action;
		}
		
		/// <summary>
		///   Update tick.
		/// </summary>
		private void UpdateTick()
		{
			UpdateTimer();
			TriggerActionCheck();
		}

		/// <summary>
		///   Update timer of action.
		/// </summary>
		private void UpdateTimer()
		{
			if(_useUnscaledDeltaTime)
			{
				_timeLeft -= Time.unscaledDeltaTime;
			}
			else
			{
				_timeLeft -= Time.deltaTime;
			}
		}

		/// <summary>
		///   Trigger action if timer ended. 
		/// </summary>
		private void TriggerActionCheck()
		{
			if(!IsTimeEnded())
			{
				return;
			}

			_action();
			Timer.Remove(this);
		}

		/// <summary>
		///   Check if timer has ended.
		/// </summary>
		/// <returns>Flag if timer has ended</returns>
		private bool IsTimeEnded()
		{
			return _timeLeft <= 0;
		}
	}
}