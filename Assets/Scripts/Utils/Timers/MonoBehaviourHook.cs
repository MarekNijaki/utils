using System;
using UnityEngine;

namespace MN.Utils.Timers
{
	/// <summary>
	///   Class to hook actions into MonoBehaviour.
	/// </summary>
	public class MonoBehaviourHook : MonoBehaviour
	{
		public Action onUpdate;

		private void Update()
		{
			if(onUpdate != null)
			{
				onUpdate();
			}
		}
	}
}