using System;
using System.Collections.Generic;
using UnityEngine;

namespace MN.Utils.Timers
{
	/// <summary>
	///   Entry point for timer actions.
	/// </summary>
	public static class Timer
	{
		/// <summary>
		///   Game object used as a flag, to determine if updater was initialized after loading scene.
		/// </summary>
		private static GameObject _isInitializedGOFlag;
		private static List<TimerAction> _timerActions;

		/// <summary>
		///   Create action that should be triggered after certain time.
		/// </summary>
		/// <param name="action">Action to trigger after time elapse</param>
		/// <param name="time">Time to wait unlit trigger action</param>
		/// <param name="name">Name of timer action</param>
		/// <param name="useUnscaledDeltaTime">Flag if timer should use unscaled delta time</param>
		/// <param name="shouldRemoveActionsWithSameName">Flag if actions of the same name, should be removed before creation of this action</param>
		/// <returns>Created timer action</returns>
		public static TimerAction Create(Action action, float time, string name = "", bool useUnscaledDeltaTime = false, bool shouldRemoveActionsWithSameName = false)
		{
			InitIfNeeded();

			if(shouldRemoveActionsWithSameName)
			{
				RemoveTimerActionsOfGivenName(name);
			}

			TimerAction timerAction = TimerAction.Create(name, useUnscaledDeltaTime, time, action);
			_timerActions.Add(timerAction);

			return timerAction;
		}

		/// <summary>
		///   Remove passed timer action.
		/// </summary>
		/// <param name="timerAction">Timer action to remove</param>
		public static void Remove(TimerAction timerAction)
		{
			InitIfNeeded();
			
			_timerActions.Remove(timerAction);
			
			if(timerAction.GO != null)
			{
				UnityEngine.Object.Destroy(timerAction.GO);
			}
		}

		/// <summary>
		///   Remove all timer actions of given name.
		/// </summary>
		/// <param name="name">Name of timer actions to remove</param>
		public static void RemoveTimerActionsOfGivenName(string name)
		{
			InitIfNeeded();
			
			for(int i = _timerActions.Count - 1; i > -1; i--)
			{
				if(_timerActions[i].Name != name)
				{
					continue;
				}
				
				Remove(_timerActions[i]);
			}
		}

		/// <summary>
		///   Initialise timer if needed.
		///   <para></para>
		///   Timer is initialised once on given scene, when first used.
		/// </summary>
		private static void InitIfNeeded()
		{
			if(_isInitializedGOFlag != null)
			{
				return;
			}
			
			_isInitializedGOFlag = new GameObject("UpdaterInitFlag");
			_timerActions = new List<TimerAction>();
		}
	}
}