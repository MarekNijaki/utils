using System;
using UnityEngine;

namespace MN.Utils.Timers
{
	/// <summary>
	///   Class to trigger actions, without creating a GameObject.
	/// </summary>
	public class TimerSimple
	{
		private readonly Action _action;
		private float _timeLeft;

		/// <summary>
		///   Constructor of simple timer. 
		/// </summary>
		/// <param name="action">Action to trigger after time elapse</param>
		/// <param name="time">Time to wait unlit trigger action</param>
		public TimerSimple(Action action, float time)
		{
			_action = action;
			_timeLeft = time;
		}

		/// <summary>
		///   Update tick.
		/// </summary>
		public void UpdateTick()
		{
			UpdateTick(Time.deltaTime);
		}

		/// <summary>
		///   Update tick.
		/// </summary>
		public void UpdateTick(float deltaTime)
		{
			UpdateTimer(deltaTime);
			TriggerActionCheck();
		}
		
		/// <summary>
		///   Update time of timer.
		/// </summary>
		/// <param name="deltaTime">Delta time that passed</param>
		private void UpdateTimer(float deltaTime)
		{
			_timeLeft -= deltaTime;
		}
		
		/// <summary>
		///   Trigger action if timer ended. 
		/// </summary>
		private void TriggerActionCheck()
		{
			if(!IsTimeEnded())
			{
				return;
			}

			_action();
		}
		
		/// <summary>
		///   Check if timer has ended.
		/// </summary>
		/// <returns>Flag if timer has ended</returns>
		private bool IsTimeEnded()
		{
			return _timeLeft <= 0;
		}
	}
}