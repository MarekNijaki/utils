namespace MN.Utils.Maths
{
	/// <summary>
	///   Struct that hold position values.
	/// </summary>
	public readonly struct Pos
	{
		public int X { get; }
		public int Y { get; }

		public Pos(int x, int y)
		{
			X = x;
			Y = y;
		}
	}
}