using UnityEngine;

namespace MN.Utils.Maths
{
	public static class MathGeometry
	{
		/// <summary>
		///   Generate random normalized direction.
		/// </summary>
		/// <returns>Random normalized direction</returns>
		public static Vector3 GetRandomDirection()
		{
			return new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;
		}

		/// <summary>
		///   Get normalized direction vector based on passed angle.
		/// </summary>
		/// <param name="angle">Angle in degrees (from 0 to 360)</param>
		/// <returns>Normalized direction vector based on passed angle</returns>
		public static Vector3 GetDirectionFromAngle(int angle)
		{
			float angleRad = angle * (Mathf.PI / 180F);
			return new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad)).normalized;
		}

		/// <summary>
		///   Get angle based on passed direction vector.
		/// </summary>
		/// <param name="direction">Direction vector</param>
		/// <returns>Angle based on passed direction vector</returns>
		public static float GetAngleFromDirection(Vector3 direction)
		{
			direction = direction.normalized;
			
			float n = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
			if(n < 0)
				n += 360;

			return n;
		}

		/// <summary>
		///   Get integer angle based on passed direction vector.
		/// </summary>
		/// <param name="direction">Direction vector</param>
		/// <returns>Integer angle based on passed direction vector</returns>
		public static int GetIntAngleFromDirection(Vector3 direction)
		{
			float angle = GetAngleFromDirection(direction);
			return Mathf.RoundToInt(angle);
		}
		
		/// <summary>
		///   Apply rotation to vector.
		/// </summary>
		/// <param name="vec">Vector to rotate</param>
		/// <param name="angle">Angle to apply</param>
		/// <returns>Vector rotated over passed angle</returns>
		public static Vector3 ApplyRotationToVector(Vector3 vec, float angle)
		{
			return Quaternion.Euler(0, 0, angle) * vec;
		}

		/// <summary>
		///   Apply rotation to vector.
		/// </summary>
		/// <param name="vec">Vector to rotate</param>
		/// <param name="rotation">Rotation vector to apply</param>
		/// <returns>Vector rotated over passed rotation</returns>
		public static Vector3 ApplyRotationToVector(Vector3 vec, Vector3 rotation)
		{
			return ApplyRotationToVector(vec, GetAngleFromDirection(rotation));
		}
	}
}