namespace MN.Utils.Parsers
{
	public static class Parser
	{
		/// <summary>
		///   Parse string into integer.
		///   <para></para>
		///   Return default value if failed.
		/// </summary>
		/// <param name="txt">Text to parse</param>
		/// <param name="defaultValue">Default value</param>
		/// <returns>Integer parsed from string</returns>
		public static int ParseStringToInt(string txt, int defaultValue = 0)
		{
			if(int.TryParse(txt, out int i))
			{
				return i;
			}

			return defaultValue;
		}

		/// <summary>
		///   Parse string into float.
		///   <para></para>
		///   Return default value if failed.
		/// </summary>
		/// <param name="txt">Text to parse</param>
		/// <param name="defaultValue">Default value</param>
		/// <returns>Float parsed from string</returns>
		public static float ParseStringToFloat(string txt, float defaultValue)
		{
			if(float.TryParse(txt, out float f))
			{
				return f;
			}

			return defaultValue;
		}
	}
}