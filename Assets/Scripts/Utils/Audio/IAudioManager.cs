using UnityEngine;

namespace MN.Utils.Audio
{
	/// <summary>
	///   Interface for audio manager.
	/// </summary>
	public interface IAudioManager
	{
		void Play(AudioClip clip);
	}
}