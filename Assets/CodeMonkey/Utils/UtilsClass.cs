﻿using System;
using System.Collections.Generic;
using MN.Utils.FontHelpers;
using MN.Utils.Timers;
using MN.Utils.UI.Buttons;
using MN.Utils.WorldPositions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace CodeMonkey.Utils
{
	public static class UtilsClass
	{
		public const int sortingOrderDefault = 5000;

		// Get Main Canvas Transform
		private static Transform cachedCanvasTransform;

		// Get Sorting order to set SpriteRenderer sortingOrder, higher position = lower sortingOrder
		public static int GetSortingOrder(Vector3 position, int offset, int baseSortingOrder = sortingOrderDefault)
		{
			return (int)(baseSortingOrder - position.y) + offset;
		}

		
		
		public static Transform GetCanvasTransform()
		{
			if(cachedCanvasTransform == null)
			{
				Canvas canvas = Object.FindObjectOfType<Canvas>();
				if(canvas != null)
				{
					cachedCanvasTransform = canvas.transform;
				}
			}
			return cachedCanvasTransform;
		}

		
		
		// Create a Sprite in the World, no parent
		public static GameObject CreateWorldSprite(string name, Sprite sprite, Vector3 position, Vector3 localScale, int sortingOrder, Color color)
		{
			return CreateWorldSprite(null, name, sprite, position, localScale, sortingOrder, color);
		}

		// Create a Sprite in the World
		public static GameObject CreateWorldSprite(Transform parent, string name, Sprite sprite, Vector3 localPosition, Vector3 localScale, int sortingOrder, Color color)
		{
			GameObject gameObject = new GameObject(name, typeof(SpriteRenderer));
			Transform transform = gameObject.transform;
			transform.SetParent(parent, false);
			transform.localPosition = localPosition;
			transform.localScale = localScale;
			SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
			spriteRenderer.sprite = sprite;
			spriteRenderer.sortingOrder = sortingOrder;
			spriteRenderer.color = color;
			return gameObject;
		}

		
		
		// Create a Sprite in the World with Button_Sprite, no parent
		public static Button_Sprite CreateWorldSpriteButton(string name, Sprite sprite, Vector3 localPosition, Vector3 localScale, int sortingOrder, Color color)
		{
			return CreateWorldSpriteButton(null, name, sprite, localPosition, localScale, sortingOrder, color);
		}

		// Create a Sprite in the World with Button_Sprite
		public static Button_Sprite CreateWorldSpriteButton(Transform parent, string name, Sprite sprite, Vector3 localPosition, Vector3 localScale, int sortingOrder, Color color)
		{
			GameObject gameObject = CreateWorldSprite(parent, name, sprite, localPosition, localScale, sortingOrder, color);
			gameObject.AddComponent<BoxCollider2D>();
			Button_Sprite buttonSprite = gameObject.AddComponent<Button_Sprite>();
			return buttonSprite;
		}

		
		
		// Creates a Text Mesh in the World and constantly updates it
		public static UpdaterAction CreateWorldTextUpdater(Func<string> GetTextFunc, Vector3 localPosition, Transform parent = null)
		{
			TextMesh textMesh = CreateWorldText(GetTextFunc(), parent, localPosition);
			return Updater.Create(() =>
			                      {
				                      textMesh.text = GetTextFunc();
				                      return false;
			                      },
			                      "WorldTextUpdater");
		}

		// Create Text in the World
		public static TextMesh CreateWorldText(string text, Transform parent = null, Vector3 localPosition = default(Vector3), int fontSize = 40, Color? color = null,
		                                       TextAnchor textAnchor = TextAnchor.UpperLeft, TextAlignment textAlignment = TextAlignment.Left, int sortingOrder = sortingOrderDefault)
		{
			if(color == null)
			{
				color = Color.white;
			}
			return CreateWorldText(parent, text, localPosition, fontSize, (Color)color, textAnchor, textAlignment, sortingOrder);
		}

		// Create Text in the World
		public static TextMesh CreateWorldText(Transform parent, string text, Vector3 localPosition, int fontSize, Color color, TextAnchor textAnchor, TextAlignment textAlignment,
		                                       int sortingOrder)
		{
			GameObject gameObject = new GameObject("World_Text", typeof(TextMesh));
			Transform transform = gameObject.transform;
			transform.SetParent(parent, false);
			transform.localPosition = localPosition;
			TextMesh textMesh = gameObject.GetComponent<TextMesh>();
			textMesh.anchor = textAnchor;
			textMesh.alignment = textAlignment;
			textMesh.text = text;
			textMesh.fontSize = fontSize;
			textMesh.color = color;
			textMesh.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;
			return textMesh;
		}

		
		
		// Create a Text Popup in the World, no parent
		public static void CreateWorldTextPopup(string text, Vector3 localPosition)
		{
			CreateWorldTextPopup(null, text, localPosition, 40, Color.white, localPosition + new Vector3(0, 20), 1f);
		}

		// Create a Text Popup in the World
		public static void CreateWorldTextPopup(Transform parent, string text, Vector3 localPosition, int fontSize, Color color, Vector3 finalPopupPosition, float popupTime)
		{
			TextMesh textMesh = CreateWorldText(parent, text, localPosition, fontSize, color, TextAnchor.LowerLeft, TextAlignment.Left, sortingOrderDefault);
			Transform transform = textMesh.transform;
			Vector3 moveAmount = (finalPopupPosition - localPosition) / popupTime;
			Updater.Create(delegate()
			               {
				               transform.position += moveAmount * Time.deltaTime;
				               popupTime -= Time.deltaTime;
				               if(popupTime <= 0f)
				               {
					               Object.Destroy(transform.gameObject);
					               return true;
				               }
				               else
				               {
					               return false;
				               }
			               },
			               "WorldTextPopup");
		}

		
		
		// Create Text Updater in UI
		public static UpdaterAction CreateUITextUpdater(Func<string> GetTextFunc, Vector2 anchoredPosition)
		{
			Text text = DrawTextUI(GetTextFunc(), anchoredPosition, 20, FontHelper.GetDefaultFont());
			return Updater.Create(() =>
			                      {
				                      text.text = GetTextFunc();
				                      return false;
			                      },
			                      "UITextUpdater");
		}

		
		
		// Draw a UI Sprite
		public static RectTransform DrawSprite(Color color, Transform parent, Vector2 pos, Vector2 size, string name = null)
		{
			RectTransform rectTransform = DrawSprite(null, color, parent, pos, size, name);
			return rectTransform;
		}

		// Draw a UI Sprite
		public static RectTransform DrawSprite(Sprite sprite, Transform parent, Vector2 pos, Vector2 size, string name = null)
		{
			RectTransform rectTransform = DrawSprite(sprite, Color.white, parent, pos, size, name);
			return rectTransform;
		}

		// Draw a UI Sprite
		public static RectTransform DrawSprite(Sprite sprite, Color color, Transform parent, Vector2 pos, Vector2 size, string name = null)
		{
			// Setup icon
			if(name == null || name == "")
			{
				name = "Sprite";
			}
			GameObject go = new GameObject(name, typeof(RectTransform), typeof(Image));
			RectTransform goRectTransform = go.GetComponent<RectTransform>();
			goRectTransform.SetParent(parent, false);
			goRectTransform.sizeDelta = size;
			goRectTransform.anchoredPosition = pos;

			Image image = go.GetComponent<Image>();
			image.sprite = sprite;
			image.color = color;

			return goRectTransform;
		}

		
		
		public static Text DrawTextUI(string textString, Vector2 anchoredPosition, int fontSize, Font font)
		{
			return DrawTextUI(textString, GetCanvasTransform(), anchoredPosition, fontSize, font);
		}

		public static Text DrawTextUI(string textString, Transform parent, Vector2 anchoredPosition, int fontSize, Font font)
		{
			GameObject textGo = new GameObject("Text", typeof(RectTransform), typeof(Text));
			textGo.transform.SetParent(parent, false);
			Transform textGoTrans = textGo.transform;
			textGoTrans.SetParent(parent, false);
			textGoTrans.localPosition = Vector3.zero;
			textGoTrans.localScale = Vector3.one;

			RectTransform textGoRectTransform = textGo.GetComponent<RectTransform>();
			textGoRectTransform.sizeDelta = new Vector2(0, 0);
			textGoRectTransform.anchoredPosition = anchoredPosition;

			Text text = textGo.GetComponent<Text>();
			text.text = textString;
			text.verticalOverflow = VerticalWrapMode.Overflow;
			text.horizontalOverflow = HorizontalWrapMode.Overflow;
			text.alignment = TextAnchor.MiddleLeft;
			if(font == null)
			{
				font = FontHelper.GetDefaultFont();
			}
			text.font = font;
			text.fontSize = fontSize;

			return text;
		}

		

		// Is Mouse over a UI Element? Used for ignoring World clicks through UI
		public static bool IsPointerOverUI()
		{
			if(EventSystem.current.IsPointerOverGameObject())
			{
				return true;
			}
			else
			{
				PointerEventData pe = new PointerEventData(EventSystem.current);
				pe.position = Input.mousePosition;
				List<RaycastResult> hits = new List<RaycastResult>();
				EventSystem.current.RaycastAll(pe, hits);
				return hits.Count > 0;
			}
		}
		
		

		public static UpdaterAction CreateMouseDraggingAction(Action<Vector3> onMouseDragging)
		{
			return CreateMouseDraggingAction(0, onMouseDragging);
		}

		public static UpdaterAction CreateMouseDraggingAction(int mouseButton, Action<Vector3> onMouseDragging)
		{
			bool dragging = false;
			return Updater.Create(() =>
			{
				if(Input.GetMouseButtonDown(mouseButton))
				{
					dragging = true;
				}
				if(Input.GetMouseButtonUp(mouseButton))
				{
					dragging = false;
				}
				if(dragging)
				{
					onMouseDragging(WorldPosition.GetMouseWorldPosition());
				}
				return false;
			});
		}

		
		
		public static UpdaterAction CreateMouseClickFromToAction(Action<Vector3, Vector3> onMouseClickFromTo, Action<Vector3, Vector3> onWaitingForToPosition)
		{
			return CreateMouseClickFromToAction(0, 1, onMouseClickFromTo, onWaitingForToPosition);
		}

		public static UpdaterAction CreateMouseClickFromToAction(int mouseButton, int cancelMouseButton, Action<Vector3, Vector3> onMouseClickFromTo,
		                                                         Action<Vector3, Vector3> onWaitingForToPosition)
		{
			int state = 0;
			Vector3 from = Vector3.zero;
			return Updater.Create(() =>
			{
				if(state == 1)
				{
					if(onWaitingForToPosition != null)
					{
						onWaitingForToPosition(@from, WorldPosition.GetMouseWorldPosition());
					}
				}
				if(state == 1 && Input.GetMouseButtonDown(cancelMouseButton))
				{
					// Cancel
					state = 0;
				}
				if(Input.GetMouseButtonDown(mouseButton) && !IsPointerOverUI())
				{
					if(state == 0)
					{
						state = 1;
						from = WorldPosition.GetMouseWorldPosition();
					}
					else
					{
						state = 0;
						onMouseClickFromTo(from, WorldPosition.GetMouseWorldPosition());
					}
				}
				return false;
			});
		}

		
		
		public static UpdaterAction CreateMouseClickAction(Action<Vector3> onMouseClick)
		{
			return CreateMouseClickAction(0, onMouseClick);
		}

		public static UpdaterAction CreateMouseClickAction(int mouseButton, Action<Vector3> onMouseClick)
		{
			return Updater.Create(() =>
			{
				if(Input.GetMouseButtonDown(mouseButton))
				{
					onMouseClick(WorldPosition.GetMouseWorldPosition());
				}
				return false;
			});
		}

		
		
		public static UpdaterAction CreateKeyCodeAction(KeyCode keyCode, Action onKeyDown)
		{
			return Updater.Create(() =>
			{
				if(Input.GetKeyDown(keyCode))
				{
					onKeyDown();
				}
				return false;
			});
		}

	}
}